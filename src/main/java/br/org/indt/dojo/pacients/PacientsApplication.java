package br.org.indt.dojo.pacients;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacientsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacientsApplication.class, args);
	}
}
