package br.org.indt.dojo.pacients.repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import br.org.indt.dojo.pacients.entities.Patient;

public interface PatientRepository extends CrudRepository<Patient, UUID> {

	List<Patient> findAll();
}

